/************************************************************************//**
* @file			ir.c
*
* @brief		This module contains the IR APIs.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*		os_evt_wait_or		disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/12/15 \n by \Sonam 
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/

/*
**===========================================================================
**		Include section
**===========================================================================
*/
#include <stdlib.h>
#include "stm32f0xx.h"
#include "std_periph_headers.h"
#include "timer.h"




	void datasend (uint16_t a, uint16_t b);
	void datasend1 (uint16_t a, uint16_t b);
	void send_data(uint16_t a);
	void add_space(uint16_t b);


	volatile uint32_t clk_cntr, chk_countr, chk_countr1,cnt,chk_countr2,chk_countr3,chk_countr4,chk_countr5,\
	chk_countr6,long_press;
	extern volatile uint8_t timer_flag, var,count,counter1,validity_ok_flag,duration_expire;


/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
 void datasend (uint16_t a, uint16_t b)
 {
 	var = 1;
	clk_cntr=0;
 	GPIO_SetBits(GPIOA,GPIO_Pin_2);				   

 	while (clk_cntr<=a);					   
 	var = 0;
	GPIO_ResetBits(GPIOA,GPIO_Pin_2);	
   	clk_cntr=0;
   	while (clk_cntr<=b);
	
 }
/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
  void datasend1 (uint16_t a, uint16_t b)
 {
 	var = 1;
	clk_cntr=0;
 	GPIO_ResetBits(GPIOA,GPIO_Pin_2);				   
	while (clk_cntr<=a);					   
 	var = 0;
	GPIO_SetBits(GPIOA,GPIO_Pin_2);	
   	clk_cntr=0;
   	while (clk_cntr<=b);
	
 }

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
 void send_data(uint16_t a)
 {
  	var = 1;
	clk_cntr=0;
 	GPIO_ResetBits(GPIOA,GPIO_Pin_2);				   
	while (clk_cntr<=a);					   
 	var = 0;

 }

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
 void add_space(uint16_t b)
 {
 	var = 0;
	GPIO_SetBits(GPIOA,GPIO_Pin_2);	
   	clk_cntr=0;
   	while (clk_cntr<=b); 
 }						 

/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/

 void transmit (uint8_t val)
{
   uint8_t loop1;												   

   for( loop1=0 ; loop1<8 ; loop1++)
   {
       if(((val<<loop1) & 0x80)==0x80)
	   	datasend(60, 160) ;
	   else
	   	datasend(60, 50);
	  // add_space(50);
	}
}


/************************************************************************//**
*					uint8_t calc_crc4(struct local_packet *field, uint8_t nob)
*
* @brief			This routine is used to validate the data packet received 
*					and also to calculate crc of the packet to be transmitted.
*
* @param *field		Pointer to the structure that contains data.
* @param nob		No. of data bytes for which crc is to be calculated.
*
* @returns			Calculated CRC value.
*
* @exception		None.
*
* @author			Aman Deep
* @date				06/06/11
* @note				None.                                       
****************************************************************************/
   void delay(uint32_t val)
     {
	   uint32_t delay_count;
	  for(delay_count=0;delay_count<val;delay_count++);
	 }

