/************************************************************************//**
* @file			json_client.c
*
* @brief		Contains  the API for packet validation from GUI
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 5/2/13 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n  
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
#include "bitops.h"
#include<stm32f0xx.h>
#include <stdio.h>
//#include "tcp.h"
#include "c_func.h"
#include "Target.h"
#include "uart.h"
//#include "buzzer.h"
//#include "client_registration.h"
//#include "water_management.h"
////#include <RTL.h>
////#include <Net_Config.h>
////#include "stm32f2xx_rtc.h"
////#include "automated_tasks.h"
//#include "AT45DB161D.h"
////#include "spi.h"
//#include "stdio.h"
//#include "string.h"
//#include "rtc.h"
//#include "logs.h"
//#include "schedule.h"
//#include "calibration.h"
//#include "sensor.h"
//#include "virtual_sensor.h"
#include"json_client.h"
//#include "memory_map.h"
//#include"timer.h"


 /*
**===========================================================================
**		structures declaration section
**===========================================================================
*/
extern serial_port json_client;
struct json_struct	json_info;

/************************************************************************//**
*	void __inline chk_circular_overflow(void)
*
* @brief			This function check the circular buffer.if buffer reach at the end 
*					then move it to starting position.				
*
* @param 			none.
*
* @returns			none
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				01/12/14
* @note				None.                                            
****************************************************************************/
void __inline chk_circular_overflow(serial_port *nw_port){
	
	if(nw_port->RxGetPtr == nw_port->InBuf + RX_BUF_LEN){
		nw_port->RxGetPtr = nw_port->InBuf;			
	} 	

}

uint8_t chk_data_format(uint8_t *ptr, uint8_t len){

	if((*ptr == '"') && (*(ptr + len - 1) == '"'))
		return 1;

	else
		return 0;	
}


/***************************************************************************************//**
*		uint8_t expression_validate(uint8_t *len)
*
* @brief			this function validates the expression in JSON format.
*				
*
* @param 			*len - length of packet  
*
* @returns			1 - if packet validates.
*					0 - otherwise
*
* @exception		None.
*
* @author			Nikhil Kukreja
* @date				06/12/14
* @note				None.                                            
*********************************************************************************************/
uint8_t expression_validate(serial_port *nw_port, uint16_t *len){
	uint8_t ret = 0, val, state = START, field_name[10], field_val[30], temp_var = 0, *ptr, sub_level = 0, server_id_not_found = 0;

	while(ret == 0){
		chk_circular_overflow(nw_port);
		val = *nw_port->RxGetPtr++;
		switch(val){
			case '{':
				ptr = field_name;
				sub_level++;
			//	state++; 
			break;
	
			case ':':
				ptr = field_val;
			//	state++;
				temp_var = 0;
			break;
	
			case ',':
				if(state == START){
					json_info.server_id = ascii_decimal(&field_val[1], temp_var - 2);		 // skip " at start and " at end
					if(json_info.server_id != JSON_SERVER_ID)
						ret = EXPRESSION_INCOMPLETE; //server_id_not_found	=	1;	
				}
				else if(state == SERVER_ID_EXTRACTED){
					json_info.slave_addr = ascii_decimal(&field_val[1], temp_var - 2);		 // skip " at start and " at end
				}
				else if(state == SAD_EXTRACTED){
					json_info.cmd = ascii_decimal(&field_val[1], temp_var - 2);		 		// skip " at start and " at end
				}
				else{
					if(!(strncmp((char *)field_name,"\"Idx\"",5))){
						json_info.idx = ascii_decimal(&field_val[1], temp_var - 2);	
					}
					else if(!(strncmp((char *)field_name,"\"idx\"",5))){
						json_info.idx = ascii_decimal(&field_val[1], temp_var - 2);	
					}
					else if(!(strncmp((char *)field_name,"\"lmt\"",5))){
						json_info.lmt = ascii_decimal(&field_val[1], temp_var - 2);	
					}	
					else{
						if(chk_data_format(&field_val[0], temp_var)){
							memcpy(&json_info.data[*len], &field_val[0], temp_var);	
							*len += temp_var;
						}
						else
							ret = EXPRESSION_INCOMPLETE;
					}
				}
				if(state < FIELD_VAL_EXTRACTED)
					state++;
				else
					state = FIELD_NAME_EXTRACTED;
				ptr = field_name;	
				temp_var = 0;
				
			break;
	
	
			case '}':
				if(state >= CMD_EXTRACTED && server_id_not_found == 0 && temp_var != 0){
					if(chk_data_format(&field_val[0], temp_var)){
						memcpy(&json_info.data[*len], &field_val[0], temp_var );	
						*len += temp_var;
						state++;
						temp_var = 0;
					}
					else{
						ret = EXPRESSION_INCOMPLETE;
					}
				}
				else if(state == SAD_EXTRACTED){
					json_info.cmd = ascii_decimal(&field_val[1], temp_var - 2);		 // skip " at start and " at end
				}
				sub_level--;
				if(sub_level == 0 && server_id_not_found == 0)
					ret = EXPRESSION_VALIDATE;
				else if(sub_level == 0 && server_id_not_found == 1)
					ret = SERVER_ID_NOT_FOUND;
			break;
	
			default:
				switch(state){
					default:
						if(sub_level == 0)
							ret = EXPRESSION_INCOMPLETE;
						else{
							*ptr++ = val;
							temp_var++;
							if(temp_var > MAX_FIELD_LEN){
								ret = EXPRESSION_INCOMPLETE;		
							}
						}
					break;
				}
			break;
	
		}
	}
	return ret;
}
