/************************************************************************//**
* @file			profile.h
*
* @brief		This module contains the profile macros & definiations.
*
* @attention	Copyright 2015 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 22/12/15 \n by \Sonam
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			 
****************************************************************************/
#ifndef __BUTTON_INPUT_H__
#define __BUTTON_INPUT_H__
/*
**===========================================================================
**		Include section
**===========================================================================
*/

#define	SINGLE_PRESS	1
#define	TWICE_PRESS		2



		/**********************************************************/
		/*						DI_SWITCH_RST_BZR				 			  */
		/**********************************************************/

#define 	SWITCH_RST_BZR_PORT          		GPIOC
#define 	SWITCH_RST_BZR_PIN			        GPIO_Pin_8



		/**********************************************************/
		/*						DI_SWITCH_PUMP1				 			  */
		/**********************************************************/

#define 	SWITCH_PUMP1_PORT          			GPIOB
#define 	SWITCH_PUMP1_PIN			        GPIO_Pin_2



		/**********************************************************/
		/*						DI_SWITCH_PUMP2				 			  */
		/**********************************************************/

#define 	SWITCH_PUMP2_PORT          			GPIOC
#define 	SWITCH_PUMP2_PIN			        GPIO_Pin_10



#define OHT_MOTOR_STATUS	GPIO_ReadInputDataBit(SWITCH_PUMP1_PORT, SWITCH_PUMP1_PIN)

#define UGT_MOTOR_STATUS	GPIO_ReadInputDataBit(SWITCH_PUMP2_PORT, SWITCH_PUMP2_PIN)




	 /**********************************************************/
	/*					   Device Profile Struct			  */
	/**********************************************************/
typedef struct {

uint8_t device_mode_state;
uint8_t button_press_count;
uint8_t button_state;
uint8_t previous_state;

}button;



 uint8_t SWITCH_PUMP1(void);
 uint8_t SWITCH_PUMP2(void);
 uint8_t SWITCH_RST_BZR(void);

 uint8_t ReadDigitalInput(GPIO_TypeDef* DI_PORT, uint16_t DI_PIN);

 void init_switch (void);
 void init_Digitalinputs (void);
 void Disable_all_Interrupt(void);
 void Disable_LPM_Interrupt(void); 
 void Disable_USB_Interrupt(void); 

#endif
