/************************************************************************//**
* @file			".h"
*
* @brief		Header for ethernet_packet.c
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 12/01/13 \n by \em Nikhil Kukreja
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
****************************************************************************/
/*
**===========================================================================
**		Include section
**===========================================================================
*/
#ifndef ACTIVATE_MODULE_H_
#define ACTIVATE_MODULE_H_

/*
**===========================================================================
**						--- macros ---
**===========================================================================
*/

//#define ENABLEWDOG    1

//#define PRIMARY_IMAGE_  1

#endif // ACTIVATE_MODULE_H_

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
	

