#ifndef __PIR_H__
#define __PIR_H__

#include "port_mapping.h"

#define PERMISSION_DENIED			0
#define PERMISSION_ALLOWED			1

#define DFLT_LOW_ACT_TIME			5	/* Minutes */
#define DFLT_MED_ACT_TIME			10	/* Minutes */
#define DFLT_HIGH_ACT_TIME			15	/* Minutes */

#define DFLT_NULL_CNT_WNDWS			4	/* Minutes */ /* Number of 15 sec windows that decide 1 null activity. */
#define DFLT_DEC_PRCNTG				15	/* Minutes */


void chk_if_pulse_to_be_sent (void);
void execute_ip_based_ops (void);
void execute_op_ops (struct ports_pool *src_pool, uint8_t state_to_be_set);
uint8_t check_fb_if_state_to_be_changed (uint8_t port, uint8_t state_to_be_set);
uint8_t check_binded_pir_input_duration (uint8_t key);
void clear_pir_input_port_parameters (struct pir_params *src_info);
void init_ip_engine (void);
void de_init_ip_engine (void);

#endif
