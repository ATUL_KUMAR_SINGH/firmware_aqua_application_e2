#ifndef __UART_H__
#define __UART_H__

#include "stm32f0xx_usart.h"
#define BAUDRATE	9600

#define USART_IDLE			0
#define USART_FILLING		1
#define USART_TRANSMITTING	2

void uart_init (USART_TypeDef* USARTx, uint32_t baud);
void uart_send_str (USART_TypeDef* USARTx, uint8_t *data_ptr, uint32_t bytes);
void uart_send_string (USART_TypeDef* USARTx, uint8_t *data_ptr); 

#define RX_BUF_LEN		1024


typedef struct{
	unsigned char		InBuf[RX_BUF_LEN];
	unsigned char		* RxPutPtr;
	unsigned char		* RxGetPtr;
//	unsigned char		OutBuf[ETH_TX_BUF_LEN];
//	unsigned char		* TxPutPtr;
//	unsigned char		* TxGetPtr;
}serial_port;

#endif
