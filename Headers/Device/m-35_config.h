   /************************************************************************//**
* @file			m-35_config.h
*
* @brief		Header for m-35_config.c
*
* @attention	Copyright 2016 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 03/10/16 \n by \em Harmeen
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
****************************************************************************/
#include <stdint.h>
#include "json_client.h"

#ifndef __M_35_CONFIG_H 
#define __M_35_CONFIG_H 

/*
**===========================================================================
**		Include section
**===========================================================================
*/

/*
**===========================================================================
**		Defines section
**===========================================================================
*/
#define CONFIG_COMMAND	 22

#define M35_MODE_AP			'1'
#define M35_MODE_STA		'2'

#define M35_PROTOCOL_NONE			'0'
#define M35_PROTOCOL_TCP_SERVER		'1'
#define M35_PROTOCOL_TCP_CLIENT		'2'
#define M35_PROTOCOL_UDP_SERVER		'3'
#define M35_PROTOCOL_UDP_CLIENT		'4'

#define M35_DHCP_STATE_ENABLE		'1'
#define M35_DHCP_STATE_DISABLE		'0'

#define M35_ENCRYPTION_METHOD_WPA_WPA2_AES		'9'

#define CONFIG_WITHOUT_PKT		1
#define CONFIG_THROUGH_PKT		2



struct wifi_init_params
{
	uint8_t local_port[4];
	uint8_t mode;
	uint8_t nw_protocol;
	uint8_t dhcp_state;
	uint8_t ip_addr[16];
	uint8_t nw_mask[16];
	uint8_t nw_gateway[16];
	uint8_t nw_dns[16];
	uint8_t sta_work_method;

	uint8_t nw_Uip[16];
	uint8_t URPort[4];

	uint8_t nw_Sssid[35];
	uint8_t nw_SKey[35];
	uint8_t nw_SEnc_Type;

	uint8_t nw_Assid[35];
	uint8_t nw_AKey[35];
	uint8_t nw_AEnc_Type;
										
};
/*
**===========================================================================
**		Function Declarations
**===========================================================================
*/
void m_35_module_config(void);
void at_command_mode_pin_config(void);
void m_35_receive(uint8_t socket_poll_number);
//void m_35_module_params_config(void);
//void m_35_module_params_config(struct json_struct *json_struct_ptr);
void m_35_module_params_config(uint8_t *json_struct_ptr, uint8_t way_of_configure);
void m_35_module_params_config_cmds_send(struct wifi_init_params *wifi_param);
void send_m_35_cmd(uint8_t *wifi_cmdstr_ptr);
//void extract_m_35_wireless_nw_params(char *strptr, struct wifi_init_params *wifi_param, char insert_char);
//void extract_m_35_wireless_nw_params(char *strptr, char insert_char);
void extract_m_35_wireless_nw_params(char *strptr, uint8_t *arrptr ,char insert_char);



#endif // 
/******************************************************************************
**                            End Of File
******************************************************************************/