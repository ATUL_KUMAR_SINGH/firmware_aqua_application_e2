/************************************************************************//**
* @file			packet.h
*
* @brief		This file contains the variables, functions and flags that are globally used.
*
* @attention	Copyright 2011 Ireo Management Pvt. Ltd.
*				All Rights Reserved.
*
* @attention	The information contained herein is confidential
*				property of Ireo. \n The use, copying, transfer or
*				disclosure of such information is prohibited except
*				by express written agreement with Ireo Management.
*
* @brief		First written on \em 06/07/11 \n by \em Seema Sharma
*
* @brief        <b> CHANGE HISTORY: </b>
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @brief		<em> dd/mm/yy - By Developer </em> \n
*				Changed _ _ _. Reason for change.
*
* @note			Module Usage details and other notes.
****************************************************************************/
#ifndef PACKET_H_
#define PACKET_H_

/*
**===========================================================================
**		Include section
**===========================================================================
*/

/*
**===========================================================================
**						--- macros ---
**===========================================================================
*/



//extern uint8_t SLAVE_ADDRESS;
//extern uint8_t packet_chk_flag;
/*
**===========================================================================
**		Define section
**===========================================================================
*/
//From stddef.h
#undef NULL
#if defined(__cplusplus)
#define NULL 0
#else
#define NULL ((void *)0)
#endif

#ifndef FALSE
#define FALSE				(0)
#endif

#ifndef TRUE
#ifdef FALSE
#define TRUE				(!FALSE)
#endif
#endif

#ifndef BIT_CLEAR
#define BIT_CLEAR				(0)
#endif

#ifndef	BIT_SET
#ifdef	BIT_CLEAR
#define	BIT_SET					(!BIT_CLEAR)
#endif
#endif


#define		BROADCAST_SLAVE_ADDRESS	0xFF

#define		PACKET_START			0xA0			/* Check Start sequence of Packets 101 */

#define		CHECK_NULL				0x00			/* Check Null */

#define		TWO_OR_MORE_BYTE		0x10			/* Receive Data differentiate for two bytes */

#define		STATUS_3BIT				0x07			/* Status of two bytes */

#define		STATUS_5BIT				0x1F			/* Status of Four bytes */

#define 	PORT_NUMBER				0xFF			/* Port Number of Micro Unit controller */

#define 	FRAME_CHECK				0xE0			/* Frame Type of message */

#define		COMMAND_4B				0x18			/* Check command for 4 bytes */

#define 	PARAMETER				0x07			/* Parameter status for 4 Byte message */

#define		RX_CRC_CHK				0x0F			/* Check receive data CRC */

#define		PACKET_LENGTH_CHK		0x18			/* Check the length of packet */

#define 	PRIORITY_CHK			0xC0			/* Check the priority of the packet */

#define		TYPE_CHK				0x38			/* Check the type of received message */

#define 	STATUS_CMND_CHK			0x07			/* Check the status & command */

#define 	NEG_ONE					-1				/* Negative one value for RxGetPtr Value if not valid */

#define		CHK_RX_GETPTR_DATA		0xFF00			/* Next byte is Not negative */

#define		CHK_RFU					0x07			/* Check RFU bits */

#define		INVALID_DATA			0xFFFF

#define		CHK_VALID_DATA			0x00FF

#define 	UNREG_SLAVE_ADDRESS		0xFF//0x01			/* Address of uUC slave device */

#define		REGISTRATION_PN			225

#define 	START_4B_OR_MORE		0xA0			/* Start bit is 101 + Len(0) + CRC(xxxx)*/

#define		START_2B				0xB0			/* Start bit is 101 + Len(1) + RFU(0)*/

#define 	PRIORITY_HIGH			0x00			/* Priority of the packet is HIGH */

#define 	PRIORITY_MEDIUM			0x40			/* Priority of the packet is MEDIUM */

#define 	PRIORITY_LOW			0x80			/* Priority of the packet is LOW */

#define 	PRIORITY_RFU			0xC0			/* Priority of the packet is RFU */

#define     FRAME_6B_TO_12B			0x40			/* Frame Type of 6 to 12 byte packets */

#define     FRAME_14B_OR_MORE		0x60			/* Frame Type of 14 &  >14 byte packets */

#define		PKT_LEN_4BYTE			3
								
#define		PKT_LEN_6BYTE			5

#define		PKT_LEN_8BYTE			7

#define		PKT_LEN_10BYTE			9

#define		PKT_LEN_12BYTE			11

#define		PKT_LEN_14BYTE			13

#define		PKT_LEN_16BYTE			15
								
#define		PKT_LEN_18BYTE			17

#define		PKT_LEN_20BYTE			19

#define		PKT_LEN_22BYTE			21

#define		PKT_LEN_24BYTE			23

#define		PKT_LEN_26BYTE			25

#define		PKT_LEN_28BYTE			27
								
#define		PKT_LEN_30BYTE			29

#define		PKT_LEN_32BYTE			31

#define 	ACK						0x05

#define 	NACK					0x02

#define		GET_MSB_DATA			0xF0

#define		GET_LSB_DATA			0x0F
/************************************/

#define		SYSTEM_SPECIFIC_PORT	247

#define		IO_SETTING_PORT			248

/************************************************************************//**
*@def			VALIDATE_START(V)
*
* @brief		Returns TRUE if Receive First Packet is Start.
****************************************************************************/
#define 	VALIDATE_START(V)				((CHECK_NULL == (PACKET_START ^ (V & 0xE0))) ? TRUE : FALSE)

/************************************************************************//**
*@def			FRAME_TWO_MORE_BYTE(V)
*
* @brief		Returns TRUE if Receive Packet length is two bytes, else FALSE
****************************************************************************/
#define		FRAME_TWO_OR_MORE_BYTE(V)		((TWO_OR_MORE_BYTE & V) ? TRUE : FALSE)	  

/************************************************************************//**
*@def			BYTE_STATUS(V)
*
* @brief		Returns Status of 2 byte Receive Packet.
****************************************************************************/
#define		STATUS_3BITS(V)					(STATUS_3BIT & V) 

/************************************************************************//**
*@def			COMPARE_SLAVE_ADDRESS(V)
*
* @brief		Returns TRUE if Receive Packet length is two bytes.
****************************************************************************/
#define		COMPARE_SLAVE_ADDRESS(V)		(CHECK_NULL == (sys_info.slave_add ^ V) ? TRUE : FALSE)

/************************************************************************//**
*@def			PORT_NO(V)
*
* @brief		Return Port Number.
****************************************************************************/
#define		PORT_NO(V)						(PORT_NUMBER & V)

/************************************************************************//**
*@def			FRAME_TYPE(V)
*
* @brief		Return Type of Frame.
****************************************************************************/ 
#define		FRAME_TYPE(V)					((FRAME_CHECK & V)>>5)	

/************************************************************************//**
*@def			STATUS_4BYTES(V)
*
* @brief		Return the status of 4 bytes.
****************************************************************************/ 
#define		STATUS_5BITS(V)					(STATUS_5BIT & V)	

/************************************************************************//**
*@def			COMMAND_4B(V)
*
* @brief		Return the command of four byte message.
****************************************************************************/ 
#define 	COMMAND_4BYTE(V)				((COMMAND_4B & V)>>3)

/************************************************************************//**
*@def			PARA_RET(V)
*
* @brief		Return the parameter value of four byte message.
****************************************************************************/
#define		PARAMETER_4BYTE(V)				(PARAMETER & V)

/************************************************************************//**
*@def			RX_CRC(V)
*
* @brief		Return the CRC value of received message.
****************************************************************************/
#define		RX_CRC(V)						(RX_CRC_CHK & V)

/************************************************************************//**
*@def			PACKET_LENGTH(V)
*
* @brief		Return the packet length of received message.
****************************************************************************/
#define		PACKET_LENGTH(V)				((PACKET_LENGTH_CHK & V)>>3)

/************************************************************************//**
*@def			PRIORITY(V)
*
* @brief		Return the priority of received message.
****************************************************************************/
#define 	PRIORITY(V)						((PRIORITY_CHK & V)>>6)

/************************************************************************//**
*@def			TYPE(V)
*
* @brief		Return the type of received message.
****************************************************************************/
#define		TYPE(V)							((TYPE_CHK & V)>>3)

/************************************************************************//**
*@def			STATUS_CMND(V)
*
* @brief		Return the status / command of received message.
****************************************************************************/
#define		STATUS_CMND(V)					(STATUS_CMND_CHK & V)

/************************************************************************//**
*@def			Next_GETPTR_BYTE(V)
*
* @brief		Return the data byte of RxGetPtr if true else false.
****************************************************************************/
#define		NEXT_GETPTR_BYTE(V)				((V != uart_no -> RxPutPtr)? * uart_no -> RxGetPtr : NEG_ONE)



/************************************************************************//**
*@def			RFU(V)	
*
* @brief		Check RFU bits
****************************************************************************/
#define		RFU(V)							(CHK_RFU & V)

#define 	RX_BUF_LEN 			1024			/* Receive Buffer for Temprory hold data receive */

#define 	TX_BUF_LEN 			256				/* Transmission Buffer for Temprory hold transmit data */

#define		PSA_BUF_LEN			8				/* Buffer for holding start addresses of last packets being transmitted */
/*
**===========================================================================
**					--- Bit-addressable Variables ---
**===========================================================================
*/

struct __attribute__((__packed__)) pkt_out_buff {
	uint8_t *packet_start_address_backup;									// Points to the starting address of packets as uploaded in OutBuf[TX_BUF_LEN]
	uint8_t	bytes_left;																		// Number of bytes left to be sent for current packet.
};

/************************************************************************//**
*@struct		serialport
*
* @brief		Structure for serialport Bit-addressable Variable bits.
****************************************************************************/
typedef struct {
	uint8_t		InBuf[RX_BUF_LEN];
	uint8_t		OutBuf[TX_BUF_LEN];
	uint8_t		* TxPutPtr;
	uint8_t		* TxGetPtr;
	uint8_t		* RxPutPtr;
	uint8_t		* RxGetPtr;
	struct 		pkt_out_buff out_pkt_info_buf[PSA_BUF_LEN]; // holds the starting addresses & number of bytes left to be transmitted for current packet.
																												// Records of upto last 8 packets may be maintained as of now. Might be increased by changing PSA_BUF_LEN.
																												
	struct 		pkt_out_buff curr_out_pkt_info;
	uint8_t		usart_state;
}serialport;

extern serialport usart1, usart2;

struct local_packet{
	uint8_t		start_byte;
	uint8_t		slave_add;
	uint8_t		port_no;
	uint8_t		frame_byte;
	uint8_t		prio_stus_cmnd_byte;
	uint8_t		data_byte[27];

};
extern struct local_packet	rx_packet_frame;
extern struct local_packet	* PTRpacket_frame;

extern struct local_packet	tx_packet_frame;
extern struct local_packet	* packet_frame_ptr;


/* Frame Byte - Status, Command, frame type, length, rfu & Parameter */
#define frame_fields(NAME)\
	union{\
		uint8_t val;\
		frame_status	 		status_4byte;\
		frame_cmnd_param	 	cmnd_param_4byte;\
		frame_length_rfu		frame_length_rfu_6or_more_bytes;\
	}NAME;

typedef struct {
	uint8_t 		status		:5;
	uint8_t 	frame_type		:3;		
}frame_status;

typedef struct {
	uint8_t 	parameter		:3;
	uint8_t 	command			:2;
	uint8_t 	frame_type		:3;
}frame_cmnd_param;

typedef struct {
	uint8_t 	rfu				:3;
	uint8_t 	length			:2;
	uint8_t 	frame_type		:3;
}frame_length_rfu;


/* Priority Byte - priority, type & status/command */
#define priority_fields(NAME)\
	union{\
		uint8_t val;\
		stscmnd_type_prio		stscmnd_type_prio_packet;\
	}NAME;

typedef struct {
	uint8_t 	status_cmnd		:3;
	uint8_t 	type			:3;
	uint8_t 	priority		:2;
}stscmnd_type_prio;

/*
**===========================================================================
**		Function Declarations
**===========================================================================
*/			
/* Calculate CRC-4 */
uint8_t calc_crc4(struct local_packet *, uint8_t);

//uint8_t keypad_crc(uint8_t * byte, uint8_t nob);

void tx_out_packet(serialport *usart, struct local_packet *out_frame, uint8_t nob);

void start_validation_decide_packet_bytes_in(serialport *uart_no);

uint16_t get_txgetptr_data(serialport *uart_no);

uint16_t get_rxgetptr_data(serialport *uart_no);

void ChkIfPacketReceived (serialport *uart_no);

void init_packet_protocol (serialport *usart);

#endif // PACKET_H_
/******************************************************************************
**                            End Of File
******************************************************************************/
