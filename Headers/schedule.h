#ifndef SCHEDULE_H
#define SCHEDULE_H

#include "stdint.h"
#include"json_client.h"


#define	TOTAL_SCHEDULE     3     //For 3 Schedule
#define DEFAULT_SCHEDULE 	 1		// For Default schedule

#ifdef FW_VER_GREATER_THAN_3_7_25
	#define MAX_SCHEDULES		3	
#else
	#define MAX_SCHEDULES		3
#endif
#define SCHEDULE_ON			1
#define SCHEDULE_OFF		0
#define SCHEDULE_ENABLE		1
#define SCHEDULE_DISABLE	2
#define SCHEDULE_DELETE		3

typedef struct __attribute__((__packed__)) {
	uint8_t schedule_stat	:2;			// Enable = 1 / Disable = 2 / Delete = 3
	uint8_t schedule_type	:1;			// Normal = 0 / Exception = 1
	uint16_t start_mins		:11;
	uint16_t stop_mins		:11;
	uint32_t day;						// day num
}schedule_mem_struct;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint16_t schedule_mins		:11;
}schedule_info;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint8_t schedule_type		:1;		// Normal = 0 / Exception = 1
	uint8_t schedule_state		:2;		// Enable = 1 / Disable = 2 / Delete = 3
	uint8_t schedule_day		:3;
	uint32_t all_schedule_days;			// 1 - 1234567
	schedule_info schd_start;
	schedule_info schd_end;
}schedule_window;

typedef struct __attribute__((__packed__)) {	// To avoid structure padding.
	uint8_t active_schedules	:4;		// Max Schedules can be 10
	uint8_t schd_running_flag	:2;		// Flag to specify if the schedule is running or not.
	schedule_window curr_schd;
}tank_schedule;


uint32_t execute_load_alarm(uint8_t tank_num);
//void save_execute_tank_schedule(struct gui_payload *wms_payload_info, uint8_t num_bytes);
uint8_t send_tank_pump_status_GUI(uint32_t tank1_status, uint32_t tank2_status, uint8_t *send_arr);

void read_schedules (uint8_t tank);
uint8_t save_schedules (struct json_struct *wms_payload_info);
void sort_alarm_buffer(schedule_window *tank_ptr, uint8_t num_of_alarms);
void configure_alarm(tank_schedule *schd, schedule_window *window);
void save_default_schedules (void);
uint8_t check_schedule_window_active (tank_schedule *tank_schedule_ptr);
void default_schedule_fetch (void) ;
#endif

/*----------------------------------------------------------------------------
 * end of file
 *---------------------------------------------------------------------------*/
