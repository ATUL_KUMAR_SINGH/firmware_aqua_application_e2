/**************************************************************************************************
 * File name:	 	m-35_config.c
 *
 * Attention:		Copyright 2016 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 03 Oct, 2016 by Harmeen Kaur
 *
 * Description: 	This module is used to configure M-35 configure. 
 *************************************************************************************************/


/**************************************************************************************************
 * Include Section
 **************************************************************************************************/
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_syscfg.h"
#include "stm32f0xx_usart.h"
#include "uart.h"
#include "system.h"
#include "m-35_config.h"
#include "std_periph_headers.h"
#include "ascii_packet.h"
#include "packet.h"
#include "eeprom.h"
#include "memory_map.h"
#include "schedule.h"
#include "buyers_n_client_reg.h"
//#include"tcp.h"

/**************************************************************************************************
* Defines section
 *************************************************************************************************/


/**************************************************************************************************
 * Global Variable Declaration Section.
 * AVOID DECLARING ANY.
 *************************************************************************************************/
 
uint8_t *string[22] =  {"at+WA=1","at+Assid=E4-App","at+Assidl=6", "at+Aam=9", "at+Apw=12345678","at+Apwl=8", "at+Aip=192,168,0,99","at+mask=255,255,255,0","at+gw=192,168,0,99","at+UType=1", "at+UIp=192.168.0.99","at+ULPort=8080", "at+URPort=8080","at+SC=0","at+SC=1","at+SC=2","at+SC=3","at+SO=1,192,168,0,99,8080,8080", "at+SO=1,192,168,0,99,8080,8080","at+SO=1,192,168,0,99,8080,8080","at+SO=1,192,168,0,99,8080,8080","at+Rb=1" };
uint8_t *read_socket_commands[4]  = {"at+SR=0,1000", "at+SR=1,1000", "at+SR=2,1000", "at+SR=3,1000"};

uint8_t *nw_strings[2] = {"E4-Neotech","123456789"};  // Assid, Akey


//uint8_t *uart_pointer;
uint8_t **pointer_socket_value;
extern uint8_t char_in_string_count, module_count;
extern serial_port json_client;
//uint8_t array_temp[500];

volatile uint8_t wifi_module_count,wifi_module_flag_set,polling_socket_flag, start_low_atcmd_pin;

uint32_t len_atcmds;
struct wifi_init_params  wifi_param_obj;

/*
**===========================================================================
**		Function Prototype Section
**===========================================================================
*/
/* Forward declaration of functions. */

void delay(uint32_t val)
{
	 uint32_t delay_count;
	 for(delay_count=0;delay_count<val;delay_count++);
}




/*
"at+WA=1",
"at+Assid=PSM-Neo",
"at+Assidl=7", 
"at+Aam=9", 
"at+Apw=123456789",
"at+Apwl=9", 
"at+Aip=192,168,0,99",
"at+mask=255,255,255,0",
"at+gw=192,168,0,99",
"at+UType=1", 
"at+UIp=192.168.0.99",   // NA
"at+ULPort=8080", 
"at+URPort=8080",        // NA
"at+SC=0",
"at+SC=1",
"at+SC=2",
"at+SC=3",
"at+SO=1,192,168,0,99,8080,8080",
"at+SO=1,192,168,0,99,8080,8080",
"at+SO=1,192,168,0,99,8080,8080",
"at+SO=1,192,168,0,99,8080,8080",
"at+Rb=1"
*/
//===========================================================================================================================
/*
	Func. Name : 	void m_35_module_params_config(struct json_struct *json_struct_ptr, uint8_t way_of_configure)

	Description :	This function 

  	@arg1 		:	struct json_struct *json_struct_ptr
	@arg2 		:	uint8_t way_of_configure
					This value can be : CONFIG_THROUGH_PKT
										CONFIG_WITHOUT_PKT
				
	@return :	None
	@author : Parvesh
	@Date: 	 04/03/2017
*/
//===========================================================================================================================
//void m_35_module_params_config(struct json_struct *json_struct_ptr)
void m_35_module_params_config(uint8_t *json_struct_ptr, uint8_t way_of_configure)
{
	uint8_t loop_count, ok_received_flag;
	uint8_t temp_arr[100], len, idx;
	uint8_t read_arr[50];

	if(way_of_configure == CONFIG_THROUGH_PKT)
		memcpy(&temp_arr[1], json_struct_ptr, 31);	// Copy all the data that arrived in packet into a local buffer
	  //memcpy(&temp_arr[0], &json_struct_ptr->data[0], 31);	// Copy all the data that arrived in packet into a local buffer
	else
	{
		;
	}
	

//-----------------------------------------------------------------------------------------
	strncpy(wifi_param_obj.local_port, "8080",4);
//	wifi_param_obj.mode = M35_MODE_AP;

	if(way_of_configure == CONFIG_THROUGH_PKT)
	{
	  wifi_param_obj.mode = temp_arr[1];
	//  eeprom_data_read_write(M35_MODE_ADDR, WRITE_OP, &wifi_param_obj.mode, 1);	
	  eeprom_data_read_write(M35_MODE_ADDR, READ_OP, &read_arr[0], 1);	// for testing purpose only
	}
	else
	{
		;
		//eeprom_data_read_write(M35_MODE_ADDR, READ_OP, &wifi_param_obj.mode, 1);	
	}

	wifi_param_obj.sta_work_method = '2';  //atul
	wifi_param_obj.nw_protocol	= M35_PROTOCOL_TCP_SERVER;
	wifi_param_obj.dhcp_state = M35_DHCP_STATE_ENABLE;

	extract_m_35_wireless_nw_params("192.168.011.254", (uint8_t *)(&wifi_param_obj.ip_addr[0]) ,',');
	extract_m_35_wireless_nw_params("255.255.255.000", (uint8_t *)(&wifi_param_obj.nw_mask[0]) ,',');
	extract_m_35_wireless_nw_params("192.168.011.001", (uint8_t *)(&wifi_param_obj.nw_gateway[0]) ,',');
	extract_m_35_wireless_nw_params("192.168.011.001", (uint8_t *)(&wifi_param_obj.nw_dns[0]) ,',');
	extract_m_35_wireless_nw_params("192.168.000.001", (uint8_t *)(&wifi_param_obj.nw_Uip[0]) ,'.');

	strncpy(wifi_param_obj.URPort, "8080",4);
//-----------------------------------------------------------------------------------------
	if(way_of_configure == CONFIG_THROUGH_PKT)
	{
		if(wifi_param_obj.mode == M35_MODE_AP)
		{
			strcpy(wifi_param_obj.nw_Assid, nw_strings[0]);
			strcpy(wifi_param_obj.nw_AKey, nw_strings[1]);
		}
		else
		{
//			idx = 1;
//			len = 0;
//			while(temp_arr[idx] != '#')	  // special character '#' declares end of string
//			{
//				idx++;	
//				len++;
//			}
		
			if(wifi_param_obj.mode == M35_MODE_AP){
//				strncpy(&wifi_param_obj.nw_Assid[0], &temp_arr[4], len);
			  //strncpy(&wifi_param_obj.nw_Assid[0], &temp_arr[1], len);
			}
			else
			{
//				strncpy(&wifi_param_obj.nw_Sssid[0], &temp_arr[4], len);
			  //strncpy(&wifi_param_obj.nw_Sssid[0], &temp_arr[1], len);
			  //eeprom_data_read_write(M35_STA_MODE_SSID_START_ADDR, WRITE_OP, &temp_arr[4], M35_SSID_MAXLEN);
			  eeprom_data_read_write(M35_STA_MODE_SSID_START_ADDR, READ_OP, &read_arr[0], M35_SSID_MAXLEN);	// for testing purpose only	
			}
		//-----------------------------------------------------------------------------------------
//			idx = 16;
//			len = 0;
//			while(temp_arr[idx] != '#')  // special character '#' declares end of string
//			{
//				idx++;	
//				len++;
//			}
		}
	
		if(wifi_param_obj.mode == M35_MODE_AP){
//			strncpy(&wifi_param_obj.nw_AKey[0], &temp_arr[21], len);
		  //strncpy(&wifi_param_obj.nw_AKey[0], &temp_arr[16], len);
		}
		else
		{
//			strncpy(&wifi_param_obj.nw_SKey[0], &temp_arr[21], len);
		  //strncpy(&wifi_param_obj.nw_SKey[0], &temp_arr[16], len);
		  //eeprom_data_read_write(M35_STA_MODE_KEY_START_ADDR, WRITE_OP, &temp_arr[21], M35_KEY_MAXLEN);
		  eeprom_data_read_write(M35_STA_MODE_KEY_START_ADDR, READ_OP, &read_arr[0], M35_KEY_MAXLEN);	// for testing purpose only	
		}
	}
   else
   {

//   		if(wifi_param_obj.mode == M35_MODE_AP)
//		{
//			//eeprom_data_read_write(M35_AP_MODE_ASSID_START_ADDR, READ_OP, &wifi_param_obj.nw_Assid[0], M35_SSID_MAXLEN);	
//	  		//eeprom_data_read_write(M35_AP_MODE_AKEY_START_ADDR, READ_OP, &wifi_param_obj.nw_AKey[0], M35_KEY_MAXLEN);	
//			
//			strcpy(wifi_param_obj.nw_Assid, "PSM-Neotech");
//			strcpy(wifi_param_obj.nw_AKey, "123456789");
//		}
//		else
//		{
//			eeprom_data_read_write(M35_STA_MODE_SSID_START_ADDR, READ_OP, &wifi_param_obj.nw_Sssid[0], M35_SSID_MAXLEN);	
//	  		eeprom_data_read_write(M35_STA_MODE_KEY_START_ADDR, READ_OP, &wifi_param_obj.nw_SKey[0], M35_KEY_MAXLEN);
//		}

   		
   }
//-----------------------------------------------------------------------------------------

	if(wifi_param_obj.mode == M35_MODE_AP)
	  wifi_param_obj.nw_AEnc_Type = M35_ENCRYPTION_METHOD_WPA_WPA2_AES;
	else
	  wifi_param_obj.nw_SEnc_Type = M35_ENCRYPTION_METHOD_WPA_WPA2_AES;

//-----------------------------------------------------------------------------------------
	if(way_of_configure == CONFIG_THROUGH_PKT)  // configurations (mode, ssid, pwd nned to be saved to eeprom)
	{
//		eeprom_data_read_write(M35_MODE_ADDR, WRITE_OP, &wifi_param_obj.mode, 1);	
	
		if(wifi_param_obj.mode == M35_MODE_AP)
		{
		  //eeprom_data_read_write(M35_AP_MODE_ASSID_START_ADDR, WRITE_OP, &wifi_param_obj.nw_Assid[0], M35_SSID_MAXLEN);	
		  //eeprom_data_read_write(M35_AP_MODE_AKEY_START_ADDR, WRITE_OP, &wifi_param_obj.nw_AKey[0], M35_KEY_MAXLEN);	
		}
	   else
	   {
//	      eeprom_data_read_write(M35_STA_MODE_SSID_START_ADDR, WRITE_OP, &wifi_param_obj.nw_Sssid[0], M35_SSID_MAXLEN);	
//		  eeprom_data_read_write(M35_STA_MODE_KEY_START_ADDR, WRITE_OP, &wifi_param_obj.nw_SKey[0], M35_KEY_MAXLEN);	   		
	   }
   }
//-----------------------------------------------------------------------------------------
	

}

//===========================================================================================================================
/*
	Func. Name : 	void m_35_module_params_config_cmds_send(struct wifi_init_params *wifi_param)

	Description :	This function 

  	@arg1 		:	struct json_struct *json_struct_ptr
				
	@return :	None
	@author : Parvesh
	@Date: 	 04/03/2017
*/
//===========================================================================================================================
void m_35_module_params_config_cmds_send(struct wifi_init_params *wifi_param)
{
	uint8_t wifi_cmdstr[50];
	//uint8_t Assidl, Assidl_ascii, Apwl, Apwl_ascii;
	uint8_t ssid_len, pw_len;
	uint8_t insert_char;
	uint8_t loop_count;
	uint8_t len;
	uint8_t temp_ascii_val[3];
//----------------------------------------------------------------------------------
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+WA=");
	strncat(&wifi_cmdstr[0], &wifi_param->mode, 1);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	if(wifi_param->mode == M35_MODE_STA)
	{
		memset(&wifi_cmdstr[0], 0 ,50);
		strcat(&wifi_cmdstr[0], "at+WM=");
		strncat(&wifi_cmdstr[0], &wifi_param->sta_work_method, 1);
		send_m_35_cmd(&wifi_cmdstr[0]);
	}
//----------------------------------------------------------------------------------

	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+dhcp=");
	strncat(&wifi_cmdstr[0], &wifi_param->dhcp_state, 1);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	memset(&wifi_cmdstr[0], 0 ,50);

	if(wifi_param->mode == M35_MODE_AP)
	{
		strcat(&wifi_cmdstr[0], "at+Assid=");
		ssid_len = strlen(&wifi_param->nw_Assid);
		strncat(&wifi_cmdstr[0], &wifi_param->nw_Assid, ssid_len);
	}
   else
   {
   		strcat(&wifi_cmdstr[0], "at+Sssid=");	
		ssid_len = strlen(&wifi_param->nw_Sssid);
		strncat(&wifi_cmdstr[0], &wifi_param->nw_Sssid, ssid_len);
   }
	
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	if(ssid_len < 10)
	 temp_ascii_val[0] = (ssid_len + 0x30);
	else if(ssid_len >= 10 && ssid_len < 100)
	{
		temp_ascii_val[0] = ((ssid_len / 10) + 0x30);
		temp_ascii_val[1] = ((ssid_len % 10) + 0x30);
	}

	memset(&wifi_cmdstr[0], 0 ,50);
	
	if(wifi_param->mode == M35_MODE_AP)
		strcat(&wifi_cmdstr[0], "at+Assidl=");
    else
		strcat(&wifi_cmdstr[0], "at+Sssidl=");

    if(ssid_len < 10)
	  strncat(&wifi_cmdstr[0], &temp_ascii_val[0], 1);
	else if(ssid_len >=10 && ssid_len < 100)
	  strncat(&wifi_cmdstr[0], &temp_ascii_val[0], 2);

	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	memset(&wifi_cmdstr[0], 0 ,50);

	if(wifi_param->mode == M35_MODE_AP)
	{
		strcat(&wifi_cmdstr[0], "at+Aam=");
		strncat(&wifi_cmdstr[0], &wifi_param->nw_AEnc_Type, 1);
	}
	else
	{
		strcat(&wifi_cmdstr[0], "at+Sam=");
		strncat(&wifi_cmdstr[0], &wifi_param->nw_SEnc_Type, 1);
	}
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	memset(&wifi_cmdstr[0], 0 ,50);

	if(wifi_param->mode == M35_MODE_AP)
	{
		strcat(&wifi_cmdstr[0], "at+Apw=");	
		pw_len = strlen(&wifi_param->nw_AKey);
		strncat(&wifi_cmdstr[0], &wifi_param->nw_AKey, pw_len);
	}
   else
   {
   		strcat(&wifi_cmdstr[0], "at+Spw=");
		pw_len = strlen(&wifi_param->nw_SKey);
		strncat(&wifi_cmdstr[0], &wifi_param->nw_SKey, pw_len);
   }

	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	if(pw_len < 10)
	 temp_ascii_val[0] = (pw_len + 0x30);
	else if(pw_len >= 10 && pw_len < 100)
	{
		temp_ascii_val[0] = ((pw_len / 10) + 0x30);
		temp_ascii_val[1] = ((pw_len % 10) + 0x30);
	}

	memset(&wifi_cmdstr[0], 0 ,50);
	
	if(wifi_param->mode == M35_MODE_AP)
	  strcat(&wifi_cmdstr[0], "at+Apwl=");
	else
	  strcat(&wifi_cmdstr[0], "at+Spwl=");
	
	if(pw_len < 10)
	 strncat(&wifi_cmdstr[0], &temp_ascii_val[0], 1);
	else
	 strncat(&wifi_cmdstr[0], &temp_ascii_val[0], 2);
	
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
if(wifi_param->dhcp_state == M35_DHCP_STATE_DISABLE || wifi_param->mode == M35_MODE_AP){
	memset(&wifi_cmdstr[0], 0 ,50);
	if(wifi_param->mode == M35_MODE_AP)
	  strcat(&wifi_cmdstr[0], "at+Aip=");
	else
	  strcat(&wifi_cmdstr[0], "at+ip=");
	//len = strlen(&wifi_param_obj.ip_addr[0]);
	//strncat(&wifi_cmdstr[0], &wifi_param_obj.ip_addr[0], len);
	strcat(&wifi_cmdstr[0], &wifi_param_obj.ip_addr[0]);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------	
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+mask=");
	//len = strlen(&wifi_param_obj.nw_mask[0]);
	//strncat(&wifi_cmdstr[0], &wifi_param_obj.nw_mask[0], len);
	strcat(&wifi_cmdstr[0], &wifi_param_obj.nw_mask[0]);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------	
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+gw=");
	//len = strlen(&wifi_param_obj.nw_gateway[0]);
	//strncat(&wifi_cmdstr[0], &wifi_param_obj.nw_gateway[0], len);
	strcat(&wifi_cmdstr[0], &wifi_param_obj.nw_gateway[0]);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------	
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+dns=");
	//len = strlen(&wifi_param_obj.nw_dns[0]);
	//strncat(&wifi_cmdstr[0], &wifi_param_obj.nw_dns[0], len);
	strcat(&wifi_cmdstr[0], &wifi_param_obj.nw_dns[0]);
	send_m_35_cmd(&wifi_cmdstr[0]);
}
//----------------------------------------------------------------------------------	
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+UType=");
	strncat(&wifi_cmdstr[0], &wifi_param->nw_protocol, 1);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------	
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+UIp=");
	//len = strlen(&wifi_param_obj.nw_Uip[0]);
	//strncat(&wifi_cmdstr[0], &wifi_param_obj.nw_Uip[0], len);
	strcat(&wifi_cmdstr[0], &wifi_param_obj.nw_Uip[0]);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+ULPort=");
	strncat(&wifi_cmdstr[0], &wifi_param->local_port, 4);
	send_m_35_cmd(&wifi_cmdstr[0]);
//----------------------------------------------------------------------------------
	memset(&wifi_cmdstr[0], 0 ,50);
	strcat(&wifi_cmdstr[0], "at+URPort=");
	strncat(&wifi_cmdstr[0], &wifi_param->URPort, 4);
	send_m_35_cmd(&wifi_cmdstr[0]);
	
	
	
////----------------------------------------------------------------------------------
////baud rate
////----------------------------------------------------------------------------------
//	memset(&wifi_cmdstr[0], 0 ,50);
//	strcat(&wifi_cmdstr[0], "at+Ub=9600");
//	send_m_35_cmd(&wifi_cmdstr[0]);
////----------------------------------------------------------------------------------
////data length
////----------------------------------------------------------------------------------
//	memset(&wifi_cmdstr[0], 0 ,50);
//	strcat(&wifi_cmdstr[0], "at+Ud=8");
//	send_m_35_cmd(&wifi_cmdstr[0]);
////----------------------------------------------------------------------------------

////----------------------------------------------------------------------------------
////Parity
////----------------------------------------------------------------------------------
//	memset(&wifi_cmdstr[0], 0 ,50);
//	strcat(&wifi_cmdstr[0], "at+Up=Up");
//	send_m_35_cmd(&wifi_cmdstr[0]);
////----------------------------------------------------------------------------------

////----------------------------------------------------------------------------------
////Stop bit
////----------------------------------------------------------------------------------
//	memset(&wifi_cmdstr[0], 0 ,50);
//	strcat(&wifi_cmdstr[0], "at+Us=Us");
//	send_m_35_cmd(&wifi_cmdstr[0]);
////----------------------------------------------------------------------------------


	send_m_35_cmd("at+SC=0");
	send_m_35_cmd("at+SC=1");
	send_m_35_cmd("at+SC=2");
	send_m_35_cmd("at+SC=3");
//----------------------------------------------------------------------------------	

	for(loop_count = 0; loop_count < 4; loop_count++)
	{
		memset(&wifi_cmdstr[0], 0 ,50);
		strcat(&wifi_cmdstr[0], "at+SO=");
		strncat(&wifi_cmdstr[0], &wifi_param->nw_protocol, 1);
		strncat(&wifi_cmdstr[0], &insert_char, 1);
		//len = strlen(&wifi_param_obj.nw_Uip[0]);
		//strncat(&wifi_cmdstr[0], &wifi_param_obj.nw_Uip[0], len);
		strcat(&wifi_cmdstr[0], &wifi_param_obj.nw_Uip[0]);
		insert_char = ',';										// ,
		strncat(&wifi_cmdstr[0], &insert_char, 1);
		strncat(&wifi_cmdstr[0], &wifi_param->URPort, 4);	// 8080
		insert_char = ',';										// ,
		strncat(&wifi_cmdstr[0], &insert_char, 1);
	
		strncat(&wifi_cmdstr[0], &wifi_param->local_port, 4);	// 8080
		
		send_m_35_cmd(&wifi_cmdstr[0]);
	}

	if(wifi_param->mode == M35_MODE_STA)
	{
		send_m_35_cmd("at+WC=1");
		module_count = 7;
		while(module_count != 0);
	}
//----------------------------------------------------------------------------------	
	send_m_35_cmd("at+Rb=1");
//----------------------------------------------------------------------------------	
}



//===========================================================================================================================
/*
	Func. Name : 	void extract_m_35_wireless_nw_params(char *strptr, uint8_t *arrptr ,char insert_char)

	Description :	This function is for making the ip address, nw mask, dns, gateway, Uip which may come in the packet in 15 chars form.
					(e.g) Packet containing ip addr: "192.168.001.015" arrives. This value needs to be converted in the following fomat for sending
					it to the wifi module with at command as : "192.168.1.15" 

  	@arg1 		:	char *strptr   ---->> pass the string whjose format is to be converted. (e.g) "192.168.001.015"
	@arg2 		:	uint8_t *arrptr --->> This will contain the address of the buffer which will now contain the converted value as : "192.168.1.15"
	@arg3 		:	char insert_char --->> This can be '.' OR ',' as desired .
				
	@return :	None
	@author : Parvesh
	@Date: 	 04/03/2017
*/
//===========================================================================================================================
//void extract_m_35_wireless_nw_params(char *strptr, struct wifi_init_params *wifi_param, char insert_char)
void extract_m_35_wireless_nw_params(char *strptr, uint8_t *arrptr ,char insert_char)
{
	uint8_t nw_param[4];
	uint8_t temp_arr[20];
	uint8_t dest_str[20];

	memset(temp_arr, 0x00, sizeof(temp_arr));
	memset(dest_str, 0x00, sizeof(dest_str));

	nw_param[0] = ascii_decimal((strptr + 0), 3); // 192.	
	nw_param[1] = ascii_decimal((strptr + 4), 3); // 168.
	nw_param[2] = ascii_decimal((strptr + 8), 3); // 1
	nw_param[3] = ascii_decimal((strptr + 12), 3); // 10

	sprintf(&temp_arr[0], "%d", nw_param[0]);
	strcat(&dest_str[0], &temp_arr[0]);
	strcat(&dest_str[0], &insert_char);

	sprintf(&temp_arr[0], "%d", nw_param[1]);
	strcat(&dest_str[0], &temp_arr[0]);
	strcat(&dest_str[0], &insert_char);

	sprintf(&temp_arr[0], "%d", nw_param[2]);
	strcat(&dest_str[0], &temp_arr[0]);
	strcat(&dest_str[0], &insert_char);

	sprintf(&temp_arr[0], "%d", nw_param[3]);
	strcat(&dest_str[0], &temp_arr[0]);		  // 192,168,1,10

	strcpy(arrptr,dest_str);
	

}


void send_m_35_cmd(uint8_t *wifi_cmdstr_ptr)
{
	uint8_t loop_count, ok_received_flag;
//	for(loop_count = 0; loop_count < 22; loop_count++)
//	 {
	     ok_received_flag = 0;
	  	 uart_send_string(USART2, &wifi_cmdstr_ptr[0]);
		 uart_send (USART2, 0x0D);
		 //char_in_string_count += 2;
		 delay(2000000);
	//	 delay(2000000);
		 while(json_client.RxGetPtr != json_client.RxPutPtr)
		 { 
		 	if((*json_client.RxGetPtr == 'o') && (*(json_client.RxGetPtr+1) == 'k'))
			{
			   json_client.RxGetPtr++;
			   
				if(json_client.RxGetPtr >= (json_client.InBuf + RX_BUF_LEN)){
					json_client.RxGetPtr = json_client.InBuf;
				}
			   ok_received_flag = 1;	
			}	
		 	else
			{
				json_client.RxGetPtr++;
				if(json_client.RxGetPtr >= (json_client.InBuf + RX_BUF_LEN)){
					json_client.RxGetPtr = json_client.InBuf;
				}	
			}
		 }
	
//	  }
}

/************************************************************************//**
* Function name: 	void m_35_module_config(void)
*
* @brief			This function configures M-35 module.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/
/*
void m_35_module_config(void)
{
 	uint8_t  ok_received_flag;
	uint8_t loop_count;
//	uint32_t len;
	
	for(loop_count = 0; loop_count < CONFIG_COMMAND; loop_count++)
	 {
	     ok_received_flag = 0;
		   len_atcmds = strlen((char *)string[loop_count]);
	  	 uart_send_str(USART2, string[loop_count],len_atcmds);
		   uart_send (USART2, 0x0D);		
			 
		 //char_in_string_count += 2;
//		 delay(3000000);
		 delay(2000000);
		 while(json_client.RxGetPtr != json_client.RxPutPtr)
		 { 
		 	if((*json_client.RxGetPtr == 'o') && (*(json_client.RxGetPtr+1) == 'k'))
			{
				
				if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN)){
					json_client.RxGetPtr = json_client.InBuf;
				}
			   json_client.RxGetPtr++;
			   ok_received_flag = 1;	
			}	
		 	else
			{
				if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN)){
					json_client.RxGetPtr = json_client.InBuf;
				}	
				json_client.RxGetPtr++;

			}
		 }
//		 if(ok_received_flag == 0)
//		 {
//		 	break;		 
//		 }
	  }  
}
*/


 /************************************************************************//**
* Function name: 	void at_command_mode(void)
*
* @brief			This function configures ES/RST pin of M-35 module.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/

void at_command_mode(void)
{
	GPIO_InitTypeDef   GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed= GPIO_Speed_Level_1;

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;	
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
		GPIO_SetBits(GPIOB, GPIO_Pin_6);
		GPIO_SetBits(GPIOB, GPIO_Pin_7);
  
}


/************************************************************************//**
* Function name: 	void wifi_enable(void)
*
* @brief			This function specifies that transfer of data will take place through wifi module.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/
/*
void wifi_enable(void)
{
   at_command_mode();
	
	
		module_count = 5;
		while(module_count != 0);
//	 GPIO_ResetBits(GPIOB, GPIO_Pin_6);
//	 start_low_atcmd_pin = 1;
//	
//	while(1)
//	{
//		if(wifi_module_flag_set)
//		{
//		 break;
//		}									   	
//	}
//	
//	GPIO_SetBits(GPIOB, GPIO_Pin_6);
//	start_low_atcmd_pin = 0;
	
	module_count = 3;
	GPIO_ResetBits(GPIOB, GPIO_Pin_6);
	while(module_count != 0);
	GPIO_SetBits(GPIOB, GPIO_Pin_6);
	module_count = 2;
	while(module_count != 0);
	
	
	
	
	m_35_module_config();
}
*/







/************************************************************************//**
* Function name: 	void m_35_receive_config(void)
*
* @brief			This function recieves data through M-35 module and sends the response of packet back.

* param:			None
*
* @returns			none
*
* @exception		None.
*
* @author			Harmeen 
* @date				03 Oct 2016			
   
****************************************************************************/
void m_35_receive(uint8_t socket_poll_number)
{
  uint8_t flag = 0, *uart_pointer = 0, char_string = 0, local_array_index = 0, uart_value_check = 0, uart_value_check_1 = 0; 
  uint16_t bytes_count=0,parcel_length = 0;
	uint32_t len;
	
	
	len = strlen((char *)read_socket_commands[socket_poll_number]);
  uart_send_str(USART2, read_socket_commands[socket_poll_number],len);
  uart_send (USART2, 0x0D);
//  delay(200000);
//  uart_pointer = json_client.RxGetPtr;
//  char_string = strlen(read_socket_commands[socket_poll_number]);
//	
//	

//	if(strncmp(uart_pointer, read_socket_commands[socket_poll_number],char_string) == 0)
//	{
//				if(uart_pointer == (json_client.InBuf + RX_BUF_LEN))
//				{
//					uart_pointer = json_client.InBuf;
//				}	
//				while(json_client.RxGetPtr != json_client.RxPutPtr)
//				 { 	
//				 	if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//					{
//						json_client.RxGetPtr = json_client.InBuf;
//					}
//				 	uart_value_check  = *json_client.RxGetPtr;
//					json_client.RxGetPtr++;
//					if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//					{
//						json_client.RxGetPtr = json_client.InBuf;
//					}
//					uart_value_check_1 = *json_client.RxGetPtr;
//				   	json_client.RxGetPtr++;	 
//					if((uart_value_check == 0x0D) && (uart_value_check_1 == 0x0A))
//					{
//			
//					   if(json_client.RxGetPtr != json_client.RxPutPtr)
//						{
//							flag = 1;
//							break;
//						}
//					}		
//				 }

//				if(flag)
//				 {
//					  uint8_t local_array[200] = {0};
//					  local_array_index = 0;
//				  	while(json_client.RxGetPtr!= json_client.RxPutPtr)
//					{
//						  
//				  		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//							{
//								  json_client.RxGetPtr = json_client.InBuf;
//							}
//							if((*json_client.RxGetPtr == ':'))
//							{
//							   if(json_client.RxGetPtr != json_client.RxPutPtr)
//								{
//									json_client.RxGetPtr++;
//									break;
//								}
//							}
//						bytes_count++;
//						local_array[local_array_index] = *json_client.RxGetPtr;
//						json_client.RxGetPtr++;
//						local_array_index++;
//					}
//			
//					  	if((local_array[0] == 'e') && (local_array[1] == 'r') && (local_array[2] == 'r'))
//						{
//							while(json_client.RxGetPtr != json_client.RxPutPtr)
//							{
//							 		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//									{
//										json_client.RxGetPtr = json_client.InBuf;
//									}
//									else
//									{
//										json_client.RxGetPtr++;
//									}		
//							}				


//						}
//						else if(local_array[0] == '0')
//						{
//						 	while(json_client.RxGetPtr != json_client.RxPutPtr)
//							{
//							 		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//									{
//										json_client.RxGetPtr = json_client.InBuf;
//									}
//									else
//									{
//										json_client.RxGetPtr++;
//									}		
//							}
//			
//						}
//						else
//						{
//						 uint8_t number_of_packets = 0, buff_check = 0; 
//						 parcel_length = ascii_decimal((uint8_t *)&local_array[0], bytes_count);
//						 uart_pointer = json_client.RxGetPtr;
//						 while(buff_check <= parcel_length)
//						 {
//						  	if(*uart_pointer == 0x0D)
//							{
//							  number_of_packets++;
//							}
//						 	uart_pointer++;
//							buff_check++;
//						 }
//						 for(buff_check = 0; buff_check <=number_of_packets;buff_check++)
//						 {
//						// packet_process();
//							json_client.RxGetPtr++;
//						 }
//						}	
//				}   				
//	}
//	else
//	{
//	  	while(json_client.RxGetPtr != json_client.RxPutPtr)
//		{
//		 		if(json_client.RxGetPtr == (json_client.InBuf + RX_BUF_LEN))
//				{
//					json_client.RxGetPtr = json_client.InBuf;
//				}
//				else
//				{
//					json_client.RxGetPtr++;
//				}		
//		}
//	}

}


