/**************************************************************************************************
 * File name:	 	uart.c
 *
 * Attention:		Copyright 2015 IREO Pvt ltd.
 * 					All rights reserved.
 *
 * Attention:		The information contained herein is confidential property of IREO.
 * 					The user, copying, transfer or disclosure of such information is
 * 					prohibited except by express written agreement with IREO.
 *
 * Brief:			First written on 30 Jan, 2015 by Sahil Saini
 *
 * Description: 	This module is used to initialize UART.
 *************************************************************************************************/


/**************************************************************************************************
 * Include Section
 **************************************************************************************************/
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_syscfg.h"
#include "stm32f0xx_usart.h"
#include "port_mapping.h"
#include "pin_config.h"
//#include "packet.h"
#include "uart.h"
#include "system.h"

#include "std_periph_headers.h"

/**************************************************************************************************
* Defines section
 *************************************************************************************************/


/**************************************************************************************************
 * Global Variable Declaration Section.
 * AVOID DECLARING ANY.
 *************************************************************************************************/
//extern serialport usart1;
extern struct system_params sys_info;
extern struct led_params led_info;
serial_port json_client;

volatile uint8_t uart_tx_flag, recv_var[10],x,flag=0 ;
uint8_t packet_process_count, packet_process_flag;
//extern uint8_t count=0	  ;

volatile uint8_t wifi_initialised;
									  
/************************************************************************//**
*				void USART1_IRQHandler(void)
*
* @brief		Interrupt service routine for Usart 1.
*				Received data is stored in InBuf & data stored in OutBuf
*				is transmitted.
*
* @param 	    None.
*
* @returns		None.
*
* @exception	None.
*
* @author		SAHIL SAINI
*
* @date			30/01/2015
*
* @note			Module Usage details and other notes.
****************************************************************************/
void USART2_IRQHandler(void) {
   
   /* If a byte is received. */
   
   if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET){
	
		 *json_client.RxPutPtr = USART_ReceiveData(USART2);		// Read the received byte from usart registers. 

		if (++json_client.RxPutPtr >= (json_client.InBuf + RX_BUF_LEN)) {		// Check for buffer end & reset if required. 
			json_client.RxPutPtr = json_client.InBuf;
		}	
/*		
		if(json_client.RxPutPtr >= (json_client.InBuf + RX_BUF_LEN)){
			json_client.RxPutPtr = json_client.InBuf;
		}
		// Read the received byte from usart registers.
		*(json_client.RxPutPtr++)  = USART_ReceiveData(USART2) ;
*/		
		if(wifi_initialised == 1) {
			packet_process_flag = 1;
			packet_process_count = 0;
		}
		USART_ClearITPendingBit (USART2, USART_IT_RXNE);
	 }
}





//void USART2_IRQHandler(void) 
// {
//   //uint16_t x;
////	static uint8_t token_recv_state = 0, token_send_state = 0, nob_prmsble_in_curr_window = 32;
////	uint8_t idx;
////
//	/* If a byte is received. */
//	    if (USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
//		 {
			/* Read the received byte from usart registers. */
//			recv_var[count++] = USART_ReceiveData(USART1) ;
//		    
//			if(count==5)
//			  {
//			     flag=1;
//				 count=0;
//			  }
			
			
//	       	USART_ClearITPendingBit (USART2, USART_IT_RXNE);
//		   // USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
//		
//		}

		/* If token's first byte is yet to be received. */
//		if (!token_recv_state) {
//			/* If the received byte is indeed the first byte of token. */
//			if(*usart1.RxPutPtr == 0xB1) {
//				/* Set the token_recv_state flag. */
//				token_recv_state = 1;
//			}
//		}
//		else {
//			if (*usart1.RxPutPtr == BROADCAST_SLAVE_ADDRESS) {
//				sys_info.brdcst_tkn_rcvd = 1;
//				/* Token has been received, clear the token_recv_state flag. */
//				token_recv_state = 0;
//			}else
//			/* If the next adjacent byte received after token start byte i.e. 0xB1. */
//			if(*usart1.RxPutPtr == sys_info.slave_add){// If my token.
//				/* Token has been received, clear the token_recv_state flag. */
//				token_recv_state = 0;
//				nob_prmsble_in_curr_window = 32;
//				/* Enable RS-485 DOUT Pin. */
//				DEN_ON;
//				/* Enable USART TX interrupt. */
//				USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
//			}
//		}
//		/* Check for buffer end & reset if required. */
//		if (++usart1.RxPutPtr >= (usart1.InBuf + RX_BUF_LEN)) {
//			usart1.RxPutPtr = usart1.InBuf;
//		}
//		USART_ClearITPendingBit (USART1, USART_IT_RXNE);
//	}else
//	/* If a USART TX interrupt has occured. */
//	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) {
//		/* If tx buff is not currently being filled. */
//		if (usart1.usart_state != USART_FILLING) {
//			/* If There is some data to be sent in outbuff. */
//			if (usart1.TxPutPtr != usart1.TxGetPtr)	{
//				/* Set the usart state to transmitting. */
//				usart1.usart_state = USART_TRANSMITTING;
//				/* Update the current packet start pointer. */
//				for (idx = 0; idx < PSA_BUF_LEN; idx++) {
//					if(usart1.out_pkt_info_buf [idx].packet_start_address_backup == usart1.TxGetPtr) {
//						usart1.curr_out_pkt_info = usart1.out_pkt_info_buf [idx];
//						break;
//					}
//				}
//				/* If number of bytes left for current packet are less than permissible bytes left for transmission in current token window. */
//				if (usart1.curr_out_pkt_info.bytes_left <= nob_prmsble_in_curr_window) {
//					/* Sending data. */
//					USART_SendData(USART1, *usart1.TxGetPtr++);
//					if (usart1.curr_out_pkt_info.bytes_left) {
//						/* To avoid rollover from 0 to 0xFF. */
//						usart1.curr_out_pkt_info.bytes_left--;
//					}
//					if (nob_prmsble_in_curr_window) {
//						/* To avoid rollover from 0 to 0xFF. */
//						nob_prmsble_in_curr_window--;
//					}
//				}else {
//					/* Nothing to be sent, set the state to idle. */
//					usart1.usart_state = USART_IDLE;
//					/* Set the token_send_state. */
//					if (!token_send_state)
//					token_send_state = 1;
//				}
//				/* Check for buffer end & reset if required. */
//				if (usart1.TxGetPtr >= (usart1.OutBuf + TX_BUF_LEN)) {
//					usart1.TxGetPtr = usart1.OutBuf;
//				}
//			}
//			else {
//				/* Nothing to be sent, set the state to idle. */
//				usart1.usart_state = USART_IDLE;
//				/* Set the token_send_state. */
//				if (!token_send_state)
//				token_send_state = 1;
//			}
//		}else {
//			/* Outbuff is currently being filled. */
//			/* Set the token_send_state. */
//			if (!token_send_state)
//			token_send_state = 1;
//		}
//		/* If first byte of token is yet to be sent. */
//		if (token_send_state == 1) {
//			/* If keypad is registered. */
//			if (sys_info.slave_add != BROADCAST_SLAVE_ADDRESS) {
//				/* Sending data. */
//				USART_SendData(USART1, 0xB3);
//				/* Set token_send_state to send the second byte. */
//				token_send_state = 2;
//			}
//			else {
//				/* Set token_send_state to token yet to be sent. */
//				token_send_state = 0;
//				/* Have to check for byte transfer completion before disabling */
//				while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
//				/* Disable RS-485 DOUT Pin. */
//				DEN_OFF;
//				/* Disable USART TX interrupt. */
//				USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
//			}
//		}else
//		/* If second byte of token is yet to be sent i.e. slave address. */
//		if (token_send_state == 2) {
//			/* Sending data. */
//			USART_SendData(USART1, sys_info.slave_add);
//			token_send_state = 0;
//			/* Have to check for byte transfer completion before disabling */
//			while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
//			/* Disable RS-485 DOUT Pin. */
//			DEN_OFF;
//			/* Disable USART TX interrupt. */
//			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
//		}
//		USART_ClearITPendingBit (USART1, USART_IT_TXE);
//	}else
//	if (USART_GetITStatus(USART1, USART_IT_ORE) == RESET) {
//		USART_ClearITPendingBit (USART1, USART_IT_ORE);
//	}
//}



/**************************************************************************************************
 * Function name: 	UART_Init(USART_TypeDef*, unsigned long)
 * Returns: 		void
 * Arguments: 		USART number, Baud rate.
 * Created by: 		
 * Date created: 	
 * Description: 	Used to initialize uart.
 * Notes:
 *************************************************************************************************/
void uart_init (USART_TypeDef* USARTx, uint32_t baud) {
	USART_InitTypeDef USART_InitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;



	if (USARTx == USART1) {
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);
		
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_9 | GPIO_Pin_10;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_1);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_1);
		GPIO_Init(GPIOA, &GPIO_InitStructure);
	}
	else if (USARTx == USART2) {
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
		
		GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_2 | GPIO_Pin_3;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_1);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_1);
	

		GPIO_Init(GPIOA, &GPIO_InitStructure);
	}

	//Configure USART setting:
	USART_InitStructure.USART_BaudRate = baud;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USARTx, &USART_InitStructure);

	if (USARTx == USART1) {
		/* Enable USART1 IRQ */
		NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}

	if (USARTx == USART2) {
		/* Enable USART2 IRQ */
		NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
		NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
	}

	USART_Cmd(USARTx,ENABLE);
	USART_ITConfig(USARTx, USART_IT_RXNE, ENABLE);

	
	json_client.RxPutPtr = json_client.InBuf;
	json_client.RxGetPtr = json_client.InBuf;

	
}

/**************************************************************************************************
 * Function name: 	void uart_send (USART_TypeDef* USARTx, uint8_t byte)
 *
 * Returns: 		None
 *
 * Arguments: 		USART_TypeDef* 		USARTx
 *					uint8_t				data
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	30 Jan, 2015
 *
 * Description: 	Used to transmit single 8-bit data.
 *
 * Notes:
 *************************************************************************************************/
void uart_send (USART_TypeDef* USARTx, uint8_t byte) {
//	while (USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
	USART_SendData(USARTx, byte);
	while (USART_GetFlagStatus(USARTx, USART_FLAG_TC) == RESET);
	USART_ClearFlag(USARTx,  USART_FLAG_TC);
	USART_ClearFlag(USARTx,  USART_FLAG_TXE);
}

/**************************************************************************************************
 * Function name: 	void uart_send_str (USART_TypeDef* USARTx, uint8_t *data_ptr, uint32_t bytes)
 *
 * Returns: 		None
 *
 * Arguments: 		USART_TypeDef* 		USARTx
 *					uint8_t				*data_ptr
 * 					uint32_t			bytes
 *
 * Created by: 		SAHIL SAINI
 *
 * Date created: 	30 Jan, 2015
 *
 * Description: 	Used to transmit single 8-bit data.
 *
 * Notes:
 *************************************************************************************************/
void uart_send_str (USART_TypeDef* USARTx, uint8_t *data_ptr, uint32_t bytes) {

	while (bytes--)
    uart_send (USARTx, *data_ptr++);

}

/**************************************************************************************************
 * Function name: 	void uart_send_string (USART_TypeDef* USARTx, uint8_t *data_ptr)
 *
 * Returns: 		None
 *
 * Arguments: 		USART_TypeDef* 		USARTx
 *					uint8_t				*data_ptr
 *
 * Created by: 		Harmeen
 *
 * Date created: 	3 Oct, 2016
 *
 * Description: 	Used to transmit string on UART
 *
 * Notes:
 *************************************************************************************************/
void uart_send_string (USART_TypeDef* USARTx, uint8_t *data_ptr) 
{

	while (*data_ptr != '\0'){
	uart_send (USARTx, *data_ptr++);
//	char_in_string_count++;
	}
}

